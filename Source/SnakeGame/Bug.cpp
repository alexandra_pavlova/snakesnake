// Fill out your copyright notice in the Description page of Project Settings.


#include "Bug.h"
#include "SnakeBase.h"

// Sets default values
ABug::ABug()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABug::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABug::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	BugMove();

}

void ABug::BugMove()
{
	if (DeltaLoc1.X != 0)
	{
		BugMoveX();
	}
	else
	{
		BugMoveY();
	}
}

void ABug::BugMoveX()
{
	FVector CurrentLoc = this->GetActorLocation();
	if (CurrentLoc.X < 550 && CurrentLoc.X > -550)
	{
		this->AddActorWorldOffset(DeltaLoc1);
	}
	else
	{
		DeltaLoc1 = DeltaLoc1*-1;
		this->AddActorWorldOffset(DeltaLoc1);
	}
}

void ABug::BugMoveY()
{
	FVector CurrentLoc = this->GetActorLocation();
	if (CurrentLoc.Y < 550 && CurrentLoc.Y > -550)
	{
		this->AddActorWorldOffset(DeltaLoc1);
	}
	else
	{
		DeltaLoc1 = DeltaLoc1*-1;
		this->AddActorWorldOffset(DeltaLoc1);
	}
}

void ABug::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->Destroy();
			this->Destroy();
		}
	}
}

