// Fill out your copyright notice in the Description page of Project Settings.


#include "FoodBooster.h"
#include "SnakeBase.h"


void AFoodBooster::Interact(AActor* Interactor, bool bIsHead)
{
	if (bIsHead)
	{
		auto Snake = Cast<ASnakeBase>(Interactor);
		if (IsValid(Snake))
		{
			Snake->SpeedChange();
			this->Destroy();
		}
	}
}
