// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/Bug.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeBug() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_ABug_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_ABug();
	ENGINE_API UClass* Z_Construct_UClass_AActor();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
	COREUOBJECT_API UScriptStruct* Z_Construct_UScriptStruct_FVector();
	SNAKEGAME_API UClass* Z_Construct_UClass_UInteractable_NoRegister();
// End Cross Module References
	void ABug::StaticRegisterNativesABug()
	{
	}
	UClass* Z_Construct_UClass_ABug_NoRegister()
	{
		return ABug::StaticClass();
	}
	struct Z_Construct_UClass_ABug_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam NewProp_DeltaLoc1_MetaData[];
#endif
		static const UE4CodeGen_Private::FStructPropertyParams NewProp_DeltaLoc1;
		static const UE4CodeGen_Private::FPropertyParamsBase* const PropPointers[];
		static const UE4CodeGen_Private::FImplementedInterfaceParams InterfaceParams[];
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_ABug_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AActor,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABug_Statics::Class_MetaDataParams[] = {
		{ "IncludePath", "Bug.h" },
		{ "ModuleRelativePath", "Bug.h" },
	};
#endif
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_ABug_Statics::NewProp_DeltaLoc1_MetaData[] = {
		{ "Category", "Bug" },
		{ "ModuleRelativePath", "Bug.h" },
	};
#endif
	const UE4CodeGen_Private::FStructPropertyParams Z_Construct_UClass_ABug_Statics::NewProp_DeltaLoc1 = { "DeltaLoc1", nullptr, (EPropertyFlags)0x0010000000010001, UE4CodeGen_Private::EPropertyGenFlags::Struct, RF_Public|RF_Transient|RF_MarkAsNative, 1, STRUCT_OFFSET(ABug, DeltaLoc1), Z_Construct_UScriptStruct_FVector, METADATA_PARAMS(Z_Construct_UClass_ABug_Statics::NewProp_DeltaLoc1_MetaData, UE_ARRAY_COUNT(Z_Construct_UClass_ABug_Statics::NewProp_DeltaLoc1_MetaData)) };
	const UE4CodeGen_Private::FPropertyParamsBase* const Z_Construct_UClass_ABug_Statics::PropPointers[] = {
		(const UE4CodeGen_Private::FPropertyParamsBase*)&Z_Construct_UClass_ABug_Statics::NewProp_DeltaLoc1,
	};
		const UE4CodeGen_Private::FImplementedInterfaceParams Z_Construct_UClass_ABug_Statics::InterfaceParams[] = {
			{ Z_Construct_UClass_UInteractable_NoRegister, (int32)VTABLE_OFFSET(ABug, IInteractable), false },
		};
	const FCppClassTypeInfoStatic Z_Construct_UClass_ABug_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<ABug>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_ABug_Statics::ClassParams = {
		&ABug::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		Z_Construct_UClass_ABug_Statics::PropPointers,
		InterfaceParams,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		UE_ARRAY_COUNT(Z_Construct_UClass_ABug_Statics::PropPointers),
		UE_ARRAY_COUNT(InterfaceParams),
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_ABug_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_ABug_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_ABug()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_ABug_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(ABug, 747297294);
	template<> SNAKEGAME_API UClass* StaticClass<ABug>()
	{
		return ABug::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_ABug(Z_Construct_UClass_ABug, &ABug::StaticClass, TEXT("/Script/SnakeGame"), TEXT("ABug"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(ABug);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
