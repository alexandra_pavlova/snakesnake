// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/GeneratedCppIncludes.h"
#include "SnakeGame/FoodBooster.h"
#ifdef _MSC_VER
#pragma warning (push)
#pragma warning (disable : 4883)
#endif
PRAGMA_DISABLE_DEPRECATION_WARNINGS
void EmptyLinkFunctionForGeneratedCodeFoodBooster() {}
// Cross Module References
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodBooster_NoRegister();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFoodBooster();
	SNAKEGAME_API UClass* Z_Construct_UClass_AFood();
	UPackage* Z_Construct_UPackage__Script_SnakeGame();
// End Cross Module References
	void AFoodBooster::StaticRegisterNativesAFoodBooster()
	{
	}
	UClass* Z_Construct_UClass_AFoodBooster_NoRegister()
	{
		return AFoodBooster::StaticClass();
	}
	struct Z_Construct_UClass_AFoodBooster_Statics
	{
		static UObject* (*const DependentSingletons[])();
#if WITH_METADATA
		static const UE4CodeGen_Private::FMetaDataPairParam Class_MetaDataParams[];
#endif
		static const FCppClassTypeInfoStatic StaticCppClassTypeInfo;
		static const UE4CodeGen_Private::FClassParams ClassParams;
	};
	UObject* (*const Z_Construct_UClass_AFoodBooster_Statics::DependentSingletons[])() = {
		(UObject* (*)())Z_Construct_UClass_AFood,
		(UObject* (*)())Z_Construct_UPackage__Script_SnakeGame,
	};
#if WITH_METADATA
	const UE4CodeGen_Private::FMetaDataPairParam Z_Construct_UClass_AFoodBooster_Statics::Class_MetaDataParams[] = {
		{ "Comment", "/**\n * \n */" },
		{ "IncludePath", "FoodBooster.h" },
		{ "ModuleRelativePath", "FoodBooster.h" },
	};
#endif
	const FCppClassTypeInfoStatic Z_Construct_UClass_AFoodBooster_Statics::StaticCppClassTypeInfo = {
		TCppClassTypeTraits<AFoodBooster>::IsAbstract,
	};
	const UE4CodeGen_Private::FClassParams Z_Construct_UClass_AFoodBooster_Statics::ClassParams = {
		&AFoodBooster::StaticClass,
		"Engine",
		&StaticCppClassTypeInfo,
		DependentSingletons,
		nullptr,
		nullptr,
		nullptr,
		UE_ARRAY_COUNT(DependentSingletons),
		0,
		0,
		0,
		0x009000A4u,
		METADATA_PARAMS(Z_Construct_UClass_AFoodBooster_Statics::Class_MetaDataParams, UE_ARRAY_COUNT(Z_Construct_UClass_AFoodBooster_Statics::Class_MetaDataParams))
	};
	UClass* Z_Construct_UClass_AFoodBooster()
	{
		static UClass* OuterClass = nullptr;
		if (!OuterClass)
		{
			UE4CodeGen_Private::ConstructUClass(OuterClass, Z_Construct_UClass_AFoodBooster_Statics::ClassParams);
		}
		return OuterClass;
	}
	IMPLEMENT_CLASS(AFoodBooster, 722097281);
	template<> SNAKEGAME_API UClass* StaticClass<AFoodBooster>()
	{
		return AFoodBooster::StaticClass();
	}
	static FCompiledInDefer Z_CompiledInDefer_UClass_AFoodBooster(Z_Construct_UClass_AFoodBooster, &AFoodBooster::StaticClass, TEXT("/Script/SnakeGame"), TEXT("AFoodBooster"), false, nullptr, nullptr, nullptr);
	DEFINE_VTABLE_PTR_HELPER_CTOR(AFoodBooster);
PRAGMA_ENABLE_DEPRECATION_WARNINGS
#ifdef _MSC_VER
#pragma warning (pop)
#endif
