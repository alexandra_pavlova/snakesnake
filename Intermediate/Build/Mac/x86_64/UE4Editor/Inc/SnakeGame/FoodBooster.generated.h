// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_FoodBooster_generated_h
#error "FoodBooster.generated.h already included, missing '#pragma once' in FoodBooster.h"
#endif
#define SNAKEGAME_FoodBooster_generated_h

#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_SPARSE_DATA
#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_RPC_WRAPPERS
#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAFoodBooster(); \
	friend struct Z_Construct_UClass_AFoodBooster_Statics; \
public: \
	DECLARE_CLASS(AFoodBooster, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodBooster)


#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAFoodBooster(); \
	friend struct Z_Construct_UClass_AFoodBooster_Statics; \
public: \
	DECLARE_CLASS(AFoodBooster, AFood, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(AFoodBooster)


#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodBooster(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(AFoodBooster) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodBooster); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodBooster); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodBooster(AFoodBooster&&); \
	NO_API AFoodBooster(const AFoodBooster&); \
public:


#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API AFoodBooster() { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API AFoodBooster(AFoodBooster&&); \
	NO_API AFoodBooster(const AFoodBooster&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, AFoodBooster); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(AFoodBooster); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(AFoodBooster)


#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_PRIVATE_PROPERTY_OFFSET
#define SnakeGame_Source_SnakeGame_FoodBooster_h_12_PROLOG
#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_RPC_WRAPPERS \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_INCLASS \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SnakeGame_Source_SnakeGame_FoodBooster_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_PRIVATE_PROPERTY_OFFSET \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_SPARSE_DATA \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_INCLASS_NO_PURE_DECLS \
	SnakeGame_Source_SnakeGame_FoodBooster_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class AFoodBooster>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SnakeGame_Source_SnakeGame_FoodBooster_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
